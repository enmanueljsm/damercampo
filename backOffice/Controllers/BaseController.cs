﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ServiciosWep.datos.Modelo;

namespace backOffice.Controllers
{
    public class BaseController : Controller
    {
        //Parcial instancia de base de datos
        public LibreriaDatos context;
        //instancia del metodo CargarDatosContext que tiene las variables: context y session
        private CargarDatosContext cargarDatosContext;

        public BaseController()
        {
            //Completa la instancia a la base de datos de la variable declarada arriba
            //Podria ser declarada aqui tambien pero debe haber otras razones para declararla aparte
            //probablemente porque hay metodos aparte de este, que tambien pueden usar dicha variable
            context = new LibreriaDatos();
        }

        public void InvalidarContext()
        {
            //Resetea los posibles datos que hayan estado almacenados en la variable context
            context = new LibreriaDatos();
        }

        public CargarDatosContext CargarDatosContext()
        {
            //CargarDatosContext: Si aun no tiene un valor(como ejemplo: al principio) 

            if (cargarDatosContext == null)
            {
                //crea una NUEVA instancia de la clase y le asigna los valores que ya existian
                cargarDatosContext = new CargarDatosContext { context = context, session = Session };
            }
            //Sino devuelve la variable exactamente como estaba.
            return cargarDatosContext;
        }


        public ActionResult Error(Exception ex)
        {
            return View("Error", ex);
        }

        public ActionResult RedirectToActionPartialView(String actionName)
        {
            return RedirectToActionPartialView(actionName, null, null);
        }

        public ActionResult RedirectToActionPartialView(String actionName, object routeValues)
        {
            return RedirectToActionPartialView(actionName, null, routeValues);
        }

        public ActionResult RedirectToActionPartialView(String actionName, String controllerName)
        {
            return RedirectToActionPartialView(actionName, controllerName, null);
        }

        public ActionResult RedirectToActionPartialView(String actionName, String controllerName, object routeValues)
        {
            var url = Url.Action(actionName, controllerName, routeValues);
            return Content("<script> window.location = '" + url + "'</script>");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Request.Params == filterContext.ActionParameters)
            {
            }

            base.OnActionExecuting(filterContext);
        }
    }

    public class CargarDatosContext
    {
        public LibreriaDatos context { get; set; }
        public HttpSessionStateBase session { get; set; }
    }
}
