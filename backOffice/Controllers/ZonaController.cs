﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using backOffice.ViewModels.Zona;
using ServiciosWep.datos.Modelo;

namespace backOffice.Controllers
{
    public class ZonaController : BaseController
    {
        public ActionResult ListZona()
        {
            var objListZonaViewModel = new ListZonaViewModel();
            objListZonaViewModel.CargarDatos(CargarDatosContext());
            return View(objListZonaViewModel);
        }

        public ActionResult EditZona(int? Id)
        {
            var EditZonaViewModel = new EditZonaViewModel();
            EditZonaViewModel.CargarDatos(CargarDatosContext(), Id);
            return View(EditZonaViewModel);
        }
        [HttpPost]
        public ActionResult EditZona(EditZonaViewModel model)
        {
            try
            {
                var modelo = !ModelState.IsValid;


                if (modelo)
                {
                    model.CargarDatos(CargarDatosContext(), model.dcz_id);
                    TryUpdateModel(model);
                    return RedirectToAction("EditZona", "Zona");
                }

                var Zonas = new dc_zona();

                Zonas = context.dc_zona.FirstOrDefault(x => x.dcz_id == model.dcz_id);

                if (Zonas == null)
                {
                    Zonas = new dc_zona();
                    Zonas.dcz_descripcion = model.dcz_descripcion;
                    Zonas.dcz_status = model.dcz_status;

                    context.dc_zona.Add(Zonas);
                }
                else
                {
                    Zonas.dcz_id = model.dcz_id;
                    Zonas.dcz_descripcion = model.dcz_descripcion;

                }
                context.SaveChanges();
                return RedirectToAction("ListZona", "Zona");

            }
            catch (Exception)
            {
                InvalidarContext();

                model.CargarDatos(CargarDatosContext(), model.dcz_id);
                TryUpdateModel(model);
                return RedirectToAction("ListZona", "Zona");
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                dc_zona Zona = context.dc_zona.Find(id);

                Zona.dcz_status = "C";

                context.SaveChanges();

                return RedirectToAction("ListZona", "Zona");
            }
            catch (Exception ex)
            {
                return RedirectToAction("ListZona", "Zona");
            }
        }

    }
}