﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using backOffice.Controllers;
using ServiciosWep.datos.Modelo;


namespace backOffice.ViewModels.Zona
{
    public class ListZonaViewModel
    {
        public List<dc_zona> ListZona { get; set; }

        public void CargarDatos(CargarDatosContext dataContext)
        {
            ListZona = dataContext.context.dc_zona.Where(x => x.dcz_status == "A").ToList();
        }

    }
}