﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using backOffice.Controllers;

namespace backOffice.ViewModels.Zona
{
    public class EditZonaViewModel
    {
        public string dcz_status { get; set; }
        public int dcz_id { get; set; }
        public string dcz_descripcion { get; set; }

        public EditZonaViewModel()
        {
        }

        public void CargarDatos(CargarDatosContext dataContext, int? Id)
        {
            var dc_Zona = dataContext.context.dc_zona.FirstOrDefault(x => x.dcz_id == Id);

            if (dc_Zona != null)
            {
                this.dcz_id = dc_Zona.dcz_id;
                this.dcz_status = dc_Zona.dcz_status;
                this.dcz_descripcion = dc_Zona.dcz_descripcion;
            }
        }
    }
}