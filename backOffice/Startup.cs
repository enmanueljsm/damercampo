﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(backOffice.Startup))]
namespace backOffice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
