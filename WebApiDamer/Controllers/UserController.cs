﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDamer.BE.respuestaUser;
using ServiciosWep.datos.Modelo;
using System.Web;

namespace WebApiDamer.Controllers
{
    
    public class UserController : ApiController
    {
        LibreriaDatos context = new LibreriaDatos();

        [HttpPost]
        [Route("users/validate")]
        public respuestaLoginC ValidarUsuario()
        {
            try
            {
                String CorreoElectronico = Convert.ToString(HttpContext.Current.Request.Form["usuario1"]);
                String Password = Convert.ToString(HttpContext.Current.Request.Form["password"]);

               

                dc_usuario objUsuario = context.dc_usuario.FirstOrDefault(x => x.dcu_usuario == CorreoElectronico &&
                                                                    x.dcu_clave == Password);

                respuestaLoginC objUser = new respuestaLoginC();

                if (objUsuario == null)
                {
                    objUser.Message = "Usuario / Clave incvalido";
                }
                else
                {

                    objUser.Error = false;
                    objUser.Data.Nombre = objUsuario.dc_auditor.dca_nombre;
                    objUser.Data.CodigoEncuestador = objUsuario.dc_auditor.dca_id;
                    objUser.Data.Token = Guid.NewGuid().ToString();
                    objUser.Message = "ok";

                    objUsuario.dcu_usuario = objUsuario.dcu_usuario;
                    objUsuario.dcu_token = objUser.Data.Token;
                    context.SaveChanges();
                }


                return objUser;
            }
            catch (Exception ex)
            {
                respuestaLoginC objUser = new respuestaLoginC();
                objUser.Error = true;
                objUser.Message = Convert.ToString(ex);
                return objUser;
            }
        }


    }
}
