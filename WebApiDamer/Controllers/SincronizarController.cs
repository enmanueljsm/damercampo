﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDamer.BE.respuestaSincronizar;
//using WebApiDamer.BE.prueba;
using ServiciosWep.datos.Modelo;
using System.Web;

namespace WebApiDamer.Controllers
{
    public class SincronizarController : ApiController
    {
        [HttpGet]
        [Route("App/Sincronizar")]
        public respuestaSincronizar Sincronizar(String Token)
        {
            try
            {
                LibreriaDatos context = new LibreriaDatos();

                dc_usuario objUser = context.dc_usuario.FirstOrDefault(x=>x.dcu_token == Token);
                if (objUser == null)
                {
                    return new respuestaSincronizar(true, "Sin Datos");
                }
                else
                {
                    dc_encuesta objControl = context.dc_encuesta.FirstOrDefault(x => x.dca_id== objUser.dca_id && x.dcen_estatus=="A");

                    if (objControl == null)
                    {
                        return new respuestaSincronizar(true, "Sin Datos");
                    }
                    else
                    {
                        respuestaSincronizar objSin = new respuestaSincronizar();

                        List<dc_encuesta_sku> ListDistrito = context.dc_encuesta_sku.Where(x => x.dcen_id == objControl.dcen_id).ToList();

                        //List<Canales> ListCanalBE = new List<Canales>();

                        //foreach (dc_canal b in ListCanal)
                        //{
                        //    Canales objCanal = new Canales();
                        //    objCanal.id = b.dcca_id;
                        //    objCanal.descripcion = b.dcca_descripcion;
                        //    ListCanalBE.Add(objCanal);
                        //}
                        //data.Error = false;
                        //data.Message = "ok";
                        //data.Canal = ListCanalBE;

                        return objSin;
                    }
                }
              
                   
                
            }
            catch (Exception ex)
            {
                respuestaSincronizar objSin = new respuestaSincronizar();
                //return new respuestaSincronizar(true, "Sin Datos");
                objSin.Error = true;
                objSin.Message = Convert.ToString(ex);
                return objSin;
            }
        }


       


    }
}
