﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDamer.BE.Fabricantes;
using ServiciosWep.datos.Modelo;

namespace WebApiDamer.Controllers
{
    public class FabricantesController : ApiController
    {

        LibreriaDatos context = new LibreriaDatos();

        [HttpGet]
        [Route("App/Fabricantes")]
        public Datos Fabricantes()
        {
            try
            {

                Datos data = new Datos();

                List<dc_fabricante> ListFab = context.dc_fabricante.Where(x => x.dcf_status == "A").ToList();
                List<Fabricantes> ListFabBE = new List<Fabricantes>();

                foreach (dc_fabricante b in ListFab)
                {
                    Fabricantes objFab = new Fabricantes();
                    objFab.id = b.dcf_id;
                    objFab.descripcion = b.dcf_descripcion;
                    ListFabBE.Add(objFab);
                }
                data.Error = false;
                data.Message = "ok";
                data.Fabricante = ListFabBE;

                return data;

            }
            catch (Exception ex)
            {
                Datos objFab = new Datos();
                objFab.Error = true;
                objFab.Message = Convert.ToString(ex);
                return objFab;
            }
        }
    }
}
