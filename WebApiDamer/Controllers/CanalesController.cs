﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDamer.BE.Canales;
using ServiciosWep.datos.Modelo;
using System.Web;

namespace WebApiDamer.Controllers
{
    public class CanalesController : ApiController
    {
        LibreriaDatos context = new LibreriaDatos();

        [HttpGet]
        [Route("App/Canales")]
        public Datos Canales()
        {
            try
            {

                Datos data = new Datos();

                List<dc_canal> ListCanal = context.dc_canal.Where(x => x.dcca_status == "A").ToList();
                List<Canales> ListCanalBE = new List<Canales>();

                foreach (dc_canal b in ListCanal)
                {
                    Canales objCanal = new Canales();
                    objCanal.id = b.dcca_id;
                    objCanal.descripcion = b.dcca_descripcion;
                    ListCanalBE.Add(objCanal);
                }
                data.Error = false;
                data.Message = "ok";
                data.Canal = ListCanalBE;

                return data;

            }
            catch (Exception ex)
            {
                Datos objFab = new Datos();
                objFab.Error = true;
                objFab.Message = Convert.ToString(ex);
                return objFab;
            }
        }
    }
}
