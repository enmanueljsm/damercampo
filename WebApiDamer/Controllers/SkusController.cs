﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDamer.BE.Skus;
using ServiciosWep.datos.Modelo;

namespace WebApiDamer.Controllers
{
    public class SkusController : ApiController
    {
        LibreriaDatos context = new LibreriaDatos();

        [HttpGet]
        [Route("App/Skus")]
        public Datos Skus()
        {
            try
            {

                Datos data = new Datos();

                List<dc_sku> ListSkus = context.dc_sku.Where(x => x.dck_estatus == "A").ToList();
                List<Skus> ListSkusBE = new List<Skus>();

                foreach (dc_sku b in ListSkus)
                {
                    Skus objSkus = new Skus();
                    
                   objSkus.dck_id= b.dck_id;
                   objSkus.dcc_id= b.dcc_id;
                   objSkus.dcsc_id= b.dcsc_id;
                   objSkus.dck_descripcion= b.dck_descripcion;
                   objSkus.dck_imagen= b.dck_imagen;
                   objSkus.dck_estatus= b.dck_estatus;
                   objSkus.dck_precio= b.dck_precio;
                   objSkus.id_dck= b.id_dck;
                   objSkus.dcum_id= b.dcum_id;
                   objSkus.ProductoId= b.ProductoId;
                   objSkus.ProductoDescripcion= b.ProductoDescripcion;
                   objSkus.SkuId= b.SkuId;
                   objSkus.SkuDescripcion= b.SkuDescripcion;
                   objSkus.SkuLegacy= b.SkuLegacy;

                   ListSkusBE.Add(objSkus);
                }
                data.Error = false;
                data.Message = "ok";
                data.Sku = ListSkusBE;

                return data;

            }
            catch (Exception ex)
            {
                Datos objFab = new Datos();
                objFab.Error = true;
                objFab.Message = Convert.ToString(ex);
                return objFab;
            }
        }
    }
}
