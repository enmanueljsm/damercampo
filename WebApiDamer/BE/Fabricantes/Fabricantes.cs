﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.Fabricantes
{
    public class Fabricantes
    {
        public int id { get; set; }
        public string descripcion { get; set; }

        public Fabricantes()
        {
        }

        public Fabricantes(int id, string descripcion)
        {
            this.id = id;
            this.descripcion = descripcion;
        }
    }
}