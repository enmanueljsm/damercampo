﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{

public class respuestaSincronizar
    {
        public bool Error { get; set; }
        public Datos Data { get; set; }
        public string Message { get; set; }


        public respuestaSincronizar()
        {
            Data = new Datos("",0,"",0,0,0,0,false,0,0);
        }

        public respuestaSincronizar(int Variables)
        {
            Data = new Datos("", 0, "", 0, 0, 0, 0, false, 0, 0);
        }

        public respuestaSincronizar(bool Error, string Message)
        {
            this.Error = Error;
            this.Message = Message;
        }
    }
}