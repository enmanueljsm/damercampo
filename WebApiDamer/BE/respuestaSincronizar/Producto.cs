﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{
    public class Producto
    {
        public string NombreProducto { get; set; }
        public int A { get; set; }
        public int C { get; set; }
        public int Total { get; set; }
        public int posArreglo { get; set; }
        public bool Autogenerado { get; set; }
        public List<Encuestas> Encuesta { get; set; }

        public Producto()
        {
           // Encuesta = new Encuestas("", 0, "", 0, 0, 0, 0, false, 0, 0);
        }

        public Producto(string Estado, int Codigo, string Descripcion, int lastCom, int actCom, int lastInv, int actInv, bool Autogenerado, int posArreglo, int precio)
        {
           // Encuesta = new Encuestas("", 0, "", 0, 0, 0, 0, false, 0, 0);
        }

        public Producto(string NombreProducto, int A, int C, int Total, int posArreglo, bool Autogenerado)
        {
            this.NombreProducto = NombreProducto;
            this.A = A;
            this.C = C;
            this.Total = Total;
            this.posArreglo = posArreglo;
            this.Autogenerado = Autogenerado;
        }
    }
}