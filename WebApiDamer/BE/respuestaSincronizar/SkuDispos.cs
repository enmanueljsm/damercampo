﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{
    public class SkuDispos
    {
        public int? inicio { get; set; }
        public int? limite { get; set; }

        public SkuDispos()
        {

        }

        public SkuDispos(int? inicio, int? limite)
        {
            this.inicio = inicio;
            this.limite = limite;
        }
    }
}