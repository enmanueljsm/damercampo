﻿using ServiciosWep.datos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{
    public class Datos
    {
        LibreriaDatos context = new LibreriaDatos();
        public PtsVenta PtsVentas { get; set; }
        public SkuDispos SkuDispo { get; set; }
        public int[] Variables { get; set; }
        public List<Distrito> Distritos { get; set; }

        public Datos(string token)
        {
         
            SkuDispo = new SkuDispos(0, 0);
            PtsVentas = new PtsVenta(0, 0);
        }
        public Datos()
        {

            SkuDispo = new SkuDispos(0, 0);
            PtsVentas = new PtsVenta(0, 0);
        }

        public Datos(string Estado, int Codigo, string Descripcion, int lastCom, int actCom, int lastInv, int actInv, bool Autogenerado, int posArreglo, int precio)
        {
            SkuDispo = new SkuDispos(0, 0);
            PtsVentas = new PtsVenta(0, 0);
        }

        public Datos(int[] Variables)
        {
            this.Variables = Variables;
        }
    }
    
    

}