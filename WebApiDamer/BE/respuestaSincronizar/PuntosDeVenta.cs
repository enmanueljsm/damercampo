﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{
    public class PuntosDeVenta
    {
        public string NombrePunto { get; set; }
        public int CodigoPunto { get; set; }
        public string TipoPunto { get; set; }
        public string DireccionA { get; set; }
        public string DireccionB { get; set; }
        public int SkuAbierta { get; set; }
        public int SkuTotales { get; set; }
        public string Estado { get; set; }
        public bool Autogenerado { get; set; }
        public int posArreglo { get; set; }
        public List<Producto> Productos { get; set; }

        public PuntosDeVenta()
        {
            //Productos = new Producto("",0,0,0,0,false);
        }

        public PuntosDeVenta(string Estado, int Codigo, string Descripcion, int lastCom, int actCom, int lastInv, int actInv, bool Autogenerado, int posArreglo, int precio)
        {
            //Productos = new Producto("", 0, 0, 0, 0, false);
        }
        
        public PuntosDeVenta(string NombrePunto, int CodigoPunto, string TipoPunto, string DireccionA, string DireccionB, int SkuAbierta, int SkuTotales, string Estado, bool Autogenerado, int posArreglo)
        {
            this.NombrePunto = NombrePunto;
            this.CodigoPunto = CodigoPunto;
            this.TipoPunto = TipoPunto;
            this.DireccionA = DireccionA;
            this.DireccionB = DireccionB;
            this.SkuAbierta = SkuAbierta;
            this.SkuTotales = SkuTotales;
            this.Estado = Estado;
            this.Autogenerado = Autogenerado;
            this.posArreglo = posArreglo;
        }

    }
}




 