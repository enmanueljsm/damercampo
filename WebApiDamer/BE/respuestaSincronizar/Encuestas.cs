﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{
    public class Encuestas
    {
        public string Estado { get; set; }
        public int Codigo { get; set; }
        public string Descripcion { get; set; }
        public int lastCom { get; set; }
        public int actCom { get; set; }
        public int lastInv { get; set; }
        public int actInv { get; set; }
        public bool Autogenerado { get; set; }
        public int posArreglo { get; set; }
        public int precio { get; set; }
        
    
        public Encuestas()
        {
        }

        public Encuestas(string Estado, int Codigo, string Descripcion, int lastCom, int actCom, int lastInv, int actInv, bool Autogenerado, int posArreglo, int precio)
        {
            this.Estado = Estado;
            this.Codigo = Codigo;
            this.Descripcion = Descripcion;
            this.lastCom = lastCom;
            this.actCom = actCom;
            this.lastInv = lastInv;
            this.actInv = actInv;
            this.Autogenerado = Autogenerado;
            this.posArreglo = posArreglo;
            this.precio = precio;
        }
    }
}

 