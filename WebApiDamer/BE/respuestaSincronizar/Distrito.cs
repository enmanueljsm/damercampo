﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaSincronizar
{
    public class Distrito
    {
        public string NombreDistrito { get; set; }
        //public string dep { get; set; }
        public int posArreglo { get; set; }
        public int A { get; set; }
        public int BT { get; set; }
        public int BD { get; set; }
        public int E { get; set; }
        public int Total { get; set; }
        public List<PuntosDeVenta> PuntosDeVentas { get; set; }

        public Distrito()
        {
           // PuntosDeVentas = new PuntosDeVenta("",0,"",0,0,0,0,false,0,0);
        }

        public Distrito(string Estado, int Codigo, string Descripcion, int lastCom, int actCom, int lastInv, int actInv, bool Autogenerado, int posArreglo, int precio)
        {
            //PuntosDeVentas = new PuntosDeVenta("", 0, "", 0, 0, 0, 0, false, 0, 0);
        }

        public Distrito(string NombreDistrito, int posArreglo, int A, int BT, int BD, int E, int Total)
        {
            this.NombreDistrito = NombreDistrito;
            this.posArreglo = posArreglo;
            this.A = A;
            this.BT = BT;
            this.BD = BD;
            this.E = E;
            this.Total = Total;
        }
    }
}