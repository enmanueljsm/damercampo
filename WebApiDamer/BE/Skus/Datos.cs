﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.Skus
{
    public class Datos
    {
        public bool Error { get; set; }
        public List<Skus> Sku { get; set; }
        public string Message { get; set; }

        public Datos()
        {

        }
        public Datos(string dck_id, string dcc_id, string dcsc_id, string dck_descripcion, string dck_imagen, string dck_estatus, string dck_precio, string id_dck, string dcum_id, string ProductoId, string ProductoDescripcion, string SkuId, string SkuDescripcion, string SkuLegacy)
        {

        }

        public Datos(bool Error, string Message)
        {
            this.Error = Error;
            this.Message = Message;
        }
    }
}