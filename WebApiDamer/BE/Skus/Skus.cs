﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.Skus
{
    public class Skus
    {
        public string dck_id { get; set; }
        public string dcc_id { get; set; }
        public string dcsc_id { get; set; }
        public string dck_descripcion { get; set; }
        public string dck_imagen { get; set; }
        public string dck_estatus { get; set; }
        public string dck_precio { get; set; }
        public string id_dck { get; set; }
        public string dcum_id { get; set; }
        public string ProductoId { get; set; }
        public string ProductoDescripcion { get; set; }
        public string SkuId { get; set; }
        public string SkuDescripcion { get; set; }
        public string SkuLegacy { get; set; }

        public Skus()
        {
        }

        public Skus(string dck_id, string dcc_id, string dcsc_id, string dck_descripcion, string dck_imagen, string dck_estatus, string dck_precio, string id_dck, string dcum_id, string ProductoId, string ProductoDescripcion, string SkuId, string SkuDescripcion, string SkuLegacy)
        {
        this.dck_id = dck_id;
        this.dcc_id = dcc_id;
        this.dcsc_id = dcsc_id;
        this.dck_descripcion = dck_descripcion;
        this.dck_imagen = dck_imagen;
        this.dck_estatus = dck_estatus;
        this.dck_precio = dck_precio;
        this.id_dck = id_dck;
        this.dcum_id = dcum_id;
        this.ProductoId = ProductoId;
        this.ProductoDescripcion = ProductoDescripcion;
        this.SkuId = SkuId;
        this.SkuDescripcion = SkuDescripcion;
        this.SkuLegacy = SkuLegacy;
        }
    }
}

 