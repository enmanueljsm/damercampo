﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.Canales
{
    public class Datos
    {
        public bool Error { get; set; }
        public List<Canales> Canal { get; set; }
        public string Message { get; set; }

        public Datos()
        {

        }
        public Datos(int id, string descripcion)
        {
        }

        public Datos(bool Error, string Message)
        {
            this.Error = Error;
            this.Message = Message;
        }
    }
}