﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.Canales
{
    public class Canales
    {
        public int id { get; set; }
        public string descripcion { get; set; }

        public Canales()
        {
        }
        
        public Canales(int id, string descripcion)
        {
            this.id = id;
            this.descripcion = descripcion;
        }
    }
}