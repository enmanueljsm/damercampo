﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDamer.BE.respuestaUser
{
    public class datos
    {
        public string Nombre { get; set; }
        public int CodigoEncuestador { get; set; }
        public string Token { get; set; }

        public datos()
        {

        }

        public datos(string Nombre, int CodigoEncuestador, string Token)
        {

            this.Nombre = Nombre;
            this.CodigoEncuestador = CodigoEncuestador;
            this.Token = Token;
        }
    }


}